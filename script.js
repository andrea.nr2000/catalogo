// script.js

document.addEventListener('DOMContentLoaded', () => {
    const buttons = document.querySelectorAll('.product-item button');

    buttons.forEach(button => {
        button.addEventListener('click', () => {
            alert(`${button.previousElementSibling.previousElementSibling.textContent} added to cart!`);
        });
    });
});
